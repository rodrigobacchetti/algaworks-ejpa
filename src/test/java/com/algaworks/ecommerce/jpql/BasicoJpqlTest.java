package com.algaworks.ecommerce.jpql;

import com.algaworks.ecommerce.EntityManagerTest;
import com.algaworks.ecommerce.dto.ProdutoDTO;
import com.algaworks.ecommerce.model.Cliente;
import com.algaworks.ecommerce.model.Pedido;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class BasicoJpqlTest extends EntityManagerTest {

    @Test
    public void projetarResultadoDTO() {
        String jpql = "select new com.algaworks.ecommerce.dto.ProdutoDTO(id, nome) from Produto";

        TypedQuery<ProdutoDTO> typedQuery = entityManager.createQuery(jpql, ProdutoDTO.class);
        List<ProdutoDTO> lista = typedQuery.getResultList();

        Assert.assertFalse(lista.isEmpty());
    }

    @Test
    public void projetarResultado() {
        String jpql = "select id, nome from Produto";

        TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
        List<Object[]> objects = typedQuery.getResultList();

        Assert.assertEquals(2, objects.get(0).length);
        objects.forEach(objects1 -> System.out.println(objects1[0] + " " + objects1[1]));
    }

    @Test
    public void selecionarUmAtributoParaRetorno() {
        String jpqlProduto = "select p.nome from Produto p";

        TypedQuery<String> typedQuery = entityManager.createQuery(jpqlProduto, String.class);
        List<String> lista = typedQuery.getResultList();
        Assert.assertEquals(String.class, lista.get(0).getClass());

        String jpqlPedido = "select p.cliente from Pedido p";
        TypedQuery<Cliente> clienteTypedQuery = entityManager.createQuery(jpqlPedido, Cliente.class);
        List<Cliente> clientes = clienteTypedQuery.getResultList();
        Assert.assertEquals(Cliente.class, clientes.get(0).getClass());
    }

    @Test
    public void buscarPorIdentificador() {
        String query = "select p from Pedido p where p.id = 1";
        TypedQuery<Pedido> typedQuery = entityManager.createQuery(query, Pedido.class);

        Pedido pedido = typedQuery.getSingleResult();
        Assert.assertNotNull(pedido);
    }

    @Test
    public void mostrarDiferencaQueries() {
        String jpql = "select p from Pedido p where p.id = 1";
        Pedido pedido;

        TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
        pedido = typedQuery.getSingleResult();
        Assert.assertNotNull(pedido);

        pedido = null;

        Query query = entityManager.createQuery(jpql);
        pedido = (Pedido) query.getSingleResult();
        Assert.assertNotNull(pedido);
    }
}
