package com.algaworks.ecommerce.mapeamentoavancado;

import com.algaworks.ecommerce.EntityManagerTest;
import com.algaworks.ecommerce.model.NotaFiscal;
import com.algaworks.ecommerce.model.Pedido;
import com.algaworks.ecommerce.model.Produto;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

public class SalvandoArquivoTest extends EntityManagerTest {

    @Test
    public void salvarXmlNota() {
        Pedido pedido = entityManager.find(Pedido.class, 1);

        NotaFiscal notaFiscal = new NotaFiscal();
        notaFiscal.setPedido(pedido);
        notaFiscal.setDataEmissao(new Date());
        notaFiscal.setXml(carregarArquivoBytes("/nota-fiscal.xml"));

        entityManager.getTransaction().begin();
        entityManager.persist(notaFiscal);
        entityManager.getTransaction().commit();

        entityManager.clear();

        NotaFiscal notaFiscalVerificacao = entityManager.find(NotaFiscal.class, notaFiscal.getId());
        Assert.assertNotNull(notaFiscalVerificacao.getXml());
        Assert.assertTrue(notaFiscalVerificacao.getXml().length > 0);

//        try {
//            OutputStream out = new FileOutputStream(
//                    Files.createFile(Paths.get(
//                            System.getProperty("user.home") + "/xml.xml")).toFile());
//            out.write(notaFiscalVerificacao.getXml());
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }

    }

    @Test
    public void salvarFotoProduto() {
        entityManager.getTransaction().begin();

        Produto produto = entityManager.find(Produto.class, 1);
        produto.setFoto(carregarArquivoBytes("/foto-kindle.jpeg"));

        entityManager.getTransaction().commit();
        entityManager.clear();

        Produto produtoCheck = entityManager.find(Produto.class, 1);
        Assert.assertNotNull(produtoCheck.getFoto());
        Assert.assertTrue(produtoCheck.getFoto().length > 0);
    }

    private static byte[] carregarArquivoBytes(String arquivo) {
        try {
            return Objects.requireNonNull(SalvandoArquivoTest.class.getResourceAsStream(
                    arquivo)).readAllBytes();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}